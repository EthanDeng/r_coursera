    corr <- function(directory, threshold = 0) {
        #  select the complete cases > threshold as the read data index (idx)
        options(digit=4)
        totalsum <- complete(directory)
        if (max(totalsum$nobs) > threshold) {
        subsum <- totalsum[totalsum$nobs > threshold,]
        idx <- subsum$id

        # with the new idx, read data into the workspace
        path <- paste(getwd(), directory, "", sep="/")
        list <- paste(path, substr(as.character(1000 + as.numeric(idx)), 2, 4), ".csv", sep="")
        myfiles = lapply(list, read.csv, sep = ",", stringsAsFactors = FALSE)
        df <- do.call("rbind", myfiles)
        newdf <- na.omit(df)

        # construct the correlation holder to save the cor
        cr <- rep(NA,length(idx))

        # calculate the cor for each i in idx
            for (i in idx) {
                # Make subset
                subdf = subset(newdf,ID==i)
                cr[i] <- cor(subdf$nitrate,subdf$sulfate)
            }
        }

        else {
            cr <- rep(NA,332)
        }
        return(as.numeric(cr[!is.na(cr)]))
    }
